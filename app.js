const bodyParser = require('body-parser');
const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const morgan = require('morgan')
const actors = require('./routes/actor');
const movies = require('./routes/movie');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(morgan('combined'))
app.use("/", express.static(path.join(__dirname, "dist/movieAng")));

mongoose.connect('mongodb+srv://dbuser:l6eASYPIUEk1dc8J44Gu56ZTH7q@cluster0-7h1s4.azure.mongodb.net/movies?retryWrites=true&w=majority', function (err) {
  if (err) {
    return console.log('Mongoose - connection error:', err);
  }
  console.log('Connect Successfully');
});

app.listen(process.env.PORT || 4000, function () {
  console.log('Your node js server is running: http://localhost:4000');
});

app.get('/', (req, res) => {
  res.send('<h1>Yolo</h1>');
});


//Configuring Endpoints
//Actor RESTFul endpoionts
app.get('/actors', actors.getAll);
app.post('/actors', actors.createOne);
app.get('/actors/:id', actors.getOne);
app.put('/actors/:id', actors.updateOne);
app.post('/actors/:id/movies', actors.addMovie);
app.delete('/actors/:id', actors.deleteOne);
app.delete('/actors/:actorId/:movieId', actors.removeMovie);

//Movie RESTFul  endpoints
app.get('/movies', movies.getAll);
app.post('/movies', movies.createOne);
app.get('/movies/:id', movies.getOne);
app.put('/movies/:id', movies.updateOne);
app.put('/movies/:movieId/:actorId', movies.addActor);
app.delete('/movies/:id', movies.deleteOne);
app.delete('/movies/:movieId/:actorId', movies.removeActor);

app.get('/movies/:year1/:year2', movies.getAllYear);
